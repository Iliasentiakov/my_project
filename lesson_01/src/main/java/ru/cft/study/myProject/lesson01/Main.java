package ru.cft.study.myProject.lesson01;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("ДЗ 01. Задача multiTable");
        System.out.print("Размер таблицы умножения AxA: ");

        // входящий параметр
        String userParameter = "";
        do {
            userParameter = scanner.nextLine();
        }
        while (checkInputParam(userParameter));
        scanner.close();

        int size = Integer.parseInt(userParameter);

        // вывод таблицы
        int finalSize = size + 1;
        int maxMultiValue = getCapacity(Math.pow(size, 2));

        IntStream.range(0, finalSize).forEach(i -> {
            // вывод результата умножения в строку по горизонтали
            IntStream.range(0, finalSize).forEach(j -> {
                int multi = j * i;
                String cellValue;
                String lineSizeFormat;
                if (multi != 0) {
                    cellValue = String.valueOf(multi);
                    lineSizeFormat = String.valueOf(maxMultiValue);
                } else if (i == 0 && j == 0) {
                    cellValue = "";
                    lineSizeFormat = String.valueOf(getCapacity(size));
                } else if (i != 0 && j == 0) {
                    cellValue = String.valueOf(i);
                    lineSizeFormat = String.valueOf(getCapacity(size));
                } else {
                    cellValue = String.valueOf(j);
                    lineSizeFormat = String.valueOf(maxMultiValue);
                }
                // выводим и заголовок столбцов и строки
                String cellFormat = "%" + lineSizeFormat + "s" + printChrExcludeLast(j, size, "|");
                System.out.printf(cellFormat, cellValue);
            });
            System.out.println();

            // вывод заголовка таблицы (столбец) подчеркивание
            System.out.print("-".repeat(getCapacity(size)) + "+");

            // вывод подчеркивания в строку по горизонтали
            IntStream.range(0, finalSize - 1).forEach(j -> {
                int multi = j * i;
                int cellSize = getCapacity(multi);
                System.out.print("-".repeat(Math.max(cellSize, maxMultiValue)) + printChrExcludeLast(j, size, "+"));
            });

            System.out.println();
        });
    }

    private static boolean checkInputParam(String userParameter) {
        boolean result = false;
        if (!userParameter.matches("\\d{2}")) {
            System.out.println("Нужно ввести только положительное число");
            result = true;
        } else if (!(Integer.parseInt(userParameter) > 0 && Integer.parseInt(userParameter) < 100)) {
            System.out.println("Целое число должно быть > 0");
            result = true;
        }
        return result;
    }

    private static int getCapacity(int size) {
        if (size == 0) {
            return 0;
        }
        return (int) Math.floor(Math.log10(size) + 1);
    }

    private static int getCapacity(double size) {
        if (size == 0) {
            return 0;
        }
        return (int) Math.floor(Math.log10(size) + 1);
    }

    private static String printChrExcludeLast(int count, int size, String str) {
        return ((count == size) ? "" : str);
    }
}
